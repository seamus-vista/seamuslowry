# Seamus Lowry

## TL;DR
- I'm located in Alexandria, VA (EST)
- I usually only work until 4pm local time
- I love to learn new, modern technologies (and will frequently refer to them as new toys)
- If you need someone to talk to about fantasy books or board games, look no further

## Introduction
The project `seamuslowry`, more informally known as "Seamus Lowry," is a neural network capable of behaving, interacting, and working in a manner indistinguishable from a human. The project is at a level of complexity that it cannot be represented in a way that `git` is able to track. Instead, it is easiest to interact with and learn about the project over Slack, Zoom, Gitlab, etc.

## Early Development

This project began in Fairfax, VA and initially worked only on training sets aimed at reaching a base level of competency in elementary and high school topics. In 2013, we determined the project would benefit from more specialized training sets and a team at the University of Virginia volunteered to take over development. They completed their work in 2016 after adding the "Bachelor of Science in Computer Science" functionality; we cannot thank them enough for their contribution. It is at this point the project was first considered to be "enterprise ready."

## Enterprise Usage

Immediately following the completion of training sets at UVA, the project was acquired by Epic Systems Corporation and began to write health record software. This necessitated a move to Madison, WI where we learned that the project could still function at full capacity in extreme cold!

After that, we decided to return the project to its home state where it was picked up by SimonComputing. Here, it worked on a variety of government contracts and honed its skills on technology like React and Spring Boot.

## Working with `seamuslowry`
This project tends to produce the best results when run in the early morning hours. It frequently starts daily tasks at 8am and finds this is the time where it is most able to write good code.

Related to this, we have also found that after 4pm local time, the project has a tendency to shut down and may become unresponsive.

With that in mind, we recommend collaborating with the project in the morning and  communicating with the project in the afternoon.

This project does support rendered video communication through Zoom, a feature added in early 2020! However, we have found that it prefers to communicate over Slack when possible. If you do wish to test the Zoom feature, please send along questions or an agenda beforehand so the project can be prepared and perform any expensive lookups prior to the meeting.

## Interests
This project appears to display preferencial treatment towards specific stimuli, they are noted here in no particular order.

### Books
The project has a voracious appetite for fantasy books, in particular books that could be described as epic fantasy. Chief among this area, books by the author known as [Brandon Sanderson](https://www.brandonsanderson.com/) are consumed almost as soon as they are produced. Other favorites of the project are Jenn Lyons' _A Chorus of Dragons_ series, James Ilsington's _Licanius_ trilogy, and Peter V. Brett's _The Demon Cycle_.

### Board Games
The project's predilection for board games is wide ranging. In no particular order, board games enjoyed by the project include:
- One Night Ultimate Werewolf
- Cash n Guns
- Settlers of Catan Cities and Knights
- Bionicle: Mask of Light (logs generated whlie playing this game indicate that it is "so abjectly bad it becomes fun if played sparingly")

### Spanish
At one point, it seems that a Spanish training set got mixed in with the the programming language training sets we were running the project against. It has not reached the same degree of proficiency in this spoken language as it has with the programmatic ones, but it enjoys the learning process.